# framstrait-etl

## Clone this repo
```
cd existing_repo
git remote add origin https://gitlab.com/qtb-hhu/qtb-sda/framstrait-etl.git
git branch -M main
git push -uf origin main
```

## Name
framstrait-etl

## Description
This repo provide an etl-pipeline 
You need to provide the folder path and the mooring.
folder_path="../data/EGC_2016-2020/"
mooring= "EGC"
You need the following files in the folder_path:
- 'Nutrients.txt',
- 'Strat.txt',
- 'IceConc.txt',
- 'IceDist.txt',
- 'IceDistPast.csv',
- 'sample_info.txt',
- 'Chl_sat.txt', 
- 'IceConcPast.csv',
- 'CTD.txt'

How to run:
main.py

```
cd data_cleaning_ras
python main.py --folder_path ../data/EGC_2016-2020/ --mooring EGC
```
## Use the GUI
just run the start_app.py script
You browser will open and you get gui, where you can choose folder and mooring via dropdown menu.

## Installation
Python 3.10
```
pip install pandas
pip install streamlit
```

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Authors and acknowledgment
Ovidiu Popa and Ellen Oldenburg

## License
MIT

## Project status
ETL

## Citation
By using this code cite:
```
@article{EO2022,
  title={After Winter reset meltwater-stratification promotes growth of 
  ice-associated phytoplankton communities in Atlantic Waters 
  of Fram Strait in spring- With implications for the 
  annual plankton succession},
  author={Ellen Oldenburg, Ovidiu Popa et al.},
  GitHub={git@gitlab.com:qtb-hhu/qtb-sda/ras-f4.git},
  year={2022}
}
```
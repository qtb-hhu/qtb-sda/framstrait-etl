import streamlit as st
import create_bac_meta_from_files

def main():
    st.set_page_config(layout="wide")
    # Designing the interface
    st.title("Data Cleaning Ras: Create bac_meta.txt")
    st.write('\n')

    st.write('You need the following files in the folder: Nutrients.txt, Strat.txt, IceConc.txt ,IceDist.txt,'
             'IceDistPast.csv, sample_info.txt, Chl_sat.txt,'
             'IceConcPast.csv ,CTD.txt')
    mooring = st.selectbox(
    'Please select the mooring.',
    ('EGC', 'F4', 'HG-IV'))

    st.write('Selected mooring:', mooring)

    folder_path = st.selectbox(
    'Select you folder path.',
    ('../data/BGC_2016-2020/', '../data/HG-IV_2016-2020/', '../data/F4_2016-2020/'))


    st.write('Selected folder_path:', folder_path)
    check_all_files = False
    if st.button("Check folder contains all files"):
        print('test')
        files = ['Nutrients.txt', 'Strat.txt', 'IceConc.txt',
                 'IceDist.txt', 'IceDistPast.csv', 'sample_info.txt',
                 'Chl_sat.txt', 'IceConcPast.csv',
                 'CTD.txt']
        check_all_files, message = create_bac_meta_from_files.check_files(folder_path, files)
        if check_all_files:
            st.write('All file are there!')
        else:
            st.write(message)
    if st.button("Create bac_meta.txt"):
        print('Hi')
        print(mooring, folder_path)
        with st.spinner('Creating file...'):
            create_bac_meta_from_files.main(folder_path, mooring)
            st.write('File created')




if __name__ == '__main__':
    main()
import pandas as pd
import numpy as np
import argparse
import os

def calculate_mean_date_over_samples(col1: pd.DataFrame, col2: pd.DataFrame, col3: pd.DataFrame, col4: pd.DataFrame):
    """
    Calculate the mean data over the provided datetime columns

    :param col1: date1 column from sample_info.txt
    :param col2: date2 column from sample_info.txt
    :param col3: date3 column from sample_info.txt
    :param col4: date4 column from sample_info.txt
    :return: mean date
    """
    df = pd.Series([col1, col2, col3, col4])
    df = pd.to_datetime(df)
    b = df.mean()
    return b

def clean_sample_info(path:str = "../data/EGC_2016-2020/"):
    """
    Clean the simple info data
    :param sample_info_path:
    :param save_path:
    :return:
    """
    # load the sample_info data
    df_sample_info = pd.read_csv(path + "sample_info.txt", sep="\t", engine="python")
    # drop the NK
    df_sample_info.drop(df_sample_info[df_sample_info['mooring_full'] == "NK"].index, inplace=True)
    # format the date columns to datetime
    for da in ["date1", "date2", "date3", "date4"]:
        df_sample_info[da] = pd.to_datetime(df_sample_info[da])
    # caclulate the main date for the samples
    df_sample_info['date'] = df_sample_info.apply(
        lambda x: calculate_mean_date_over_samples(x.date1, x.date2, x.date3, x.date4), axis=1)
    df_sample_info = df_sample_info[df_sample_info["locus_tag"] == "16S"]
    save_path: str = path + "clean_simple_info.txt"
    df_sample_info.to_csv(save_path, sep="\t")
    return save_path

def is_egc(col: pd.DataFrame) -> bool:
    """
    Function to check if the mooring is correct or not.
    :param col: pandas dataframe column
    :return: Bool
    """
    if col in ["EGC-3", "EGC-4", "EGC-5", "EGC-6", np.nan]:
        return True
    else:
        return False


def is_f4(col: pd.DataFrame) -> bool:
    """
    Function to check if the mooring is correct or not.
    :param col: pandas dataframe column
    :return: Bool
    """
    if col in ['F4-S-1', 'F4-S-2', 'F4-S-3', 'F4-S-4', np.nan]:
        return True
    else:
        return False


def is_hg(col: pd.DataFrame) -> bool:
    """
    Function to check if the mooring is correct or not.
    :param col: pandas dataframe column
    :return: Bool
    """
    if col in ['HG-IV-S-1', 'HG-IV-S-2', 'HG-IV-S-3', 'HG-IV-S-4', np.nan]:
        return True
    else:
        return False

def create_abt_all_moor(mod:str = "EGC", path:str = "../data/EGC_2016-2020/", clean_sample_info:str = "../data/EGC_2016-2020/clean_simple_info.txt"):
    # data frame for CTD data
    df_CTD = pd.read_csv(path + "CTD.txt", sep="\t", engine="python")

    # data frame forIce_Conc
    df_Ice_Conc = pd.read_csv(path + "IceConc.txt", sep="\t", engine="python")[["date", mod]]
    df_Ice_Conc.columns = ["date", "iceConc"]

    # data frame for the Chl_sat
    df_Chl_sat = pd.read_csv(path + "Chl_sat.txt", sep="\t", engine="python")[["date", mod]]
    df_Chl_sat.columns = ["date", "Chl_sat"]

    # data frame for the IceDist
    df_IceDist = pd.read_csv(path + "IceDist.txt", sep="\t", engine="python")[["date", mod]]
    df_IceDist.columns = ["date", "iceDist"]

    # data frame for the IceDistPast
    df_IceDistPast = pd.read_csv(path + "IceDistPast.csv", sep=";", engine="python", decimal=',')
    df_IceDistPast.columns =['RAS_id', 'icePast']
    # data frame for the IceConcPast
    df_IceConcPast = pd.read_csv(path + "IceConcPast.csv", sep=";", engine="python", decimal=',')
    df_IceConcPast.columns =['RAS_id', 'iceDistPast']
    # merge past data
    df_past = pd.merge(df_IceDistPast, df_IceConcPast, how='inner', on='RAS_id')
    #print(df_past.head())

    # map ras_id based icePast and iceDistPast to date1
    df_sample_info = pd.read_csv(clean_sample_info, sep="\t", engine="python")
    df_sample_info = df_sample_info[df_sample_info["mooring"] == mod]
    df_sample_info = df_sample_info[df_sample_info["locus_tag"] == "16S"]
    df_past_time = pd.merge(df_past, df_sample_info[['RAS_id', 'date1']], how='inner', on='RAS_id')
    df_past_time = df_past_time[['date1', 'icePast', 'iceDistPast']]
    df_past_time.columns= ['date', 'icePast', 'iceDistPast']
    #print(df_past_time.head())

    # data frame for the nutrients
    df_Nutrients = pd.read_csv(path + "Nutrients.txt", sep="\t", engine="python")
    if mod == "EGC":
        df_Nutrients = df_Nutrients[df_Nutrients['mooring_full'] == mod + "-3"]
    elif mod == "F4":
        df_Nutrients = df_Nutrients[df_Nutrients['mooring_full'] == mod + "-S-1"]
    elif mod == "HG-IV":
        df_Nutrients = df_Nutrients[df_Nutrients['mooring_full'] == mod + "-S-1"]
    df_Nutrients = df_Nutrients[['date', 'NO3_NO2', 'PO4', 'NO2', 'SiO4']]
    # We have not Strat data for EGC
    if mod != "EGC":
        df_Strat = pd.read_csv(path + "Strat.txt", sep="\t", engine="python")
        if mod == "F4":
            df_Strat = df_Strat[df_Strat['mooring'] == mod]
        elif mod == "HG-IV":
            df_Strat = df_Strat[df_Strat['mooring'] == mod]
        df_Strat = df_Strat[['date',
                             'depth (uppermost measurement)', 'depth (second to top measurement)',
                             'density (uppermost depth)', 'density (2nd depth)',
                             'stratification_N^2',
                             'contribution to stratification N^2 due to temperature',
                             'contribution to stratification N^2 due to salinity', 'MLD']]

    # merge the data
    df_abt = pd.merge(df_CTD, df_Ice_Conc, how='outer', on='date')
    df_abt = pd.merge(df_abt, df_Chl_sat, how='outer', on='date')
    df_abt = pd.merge(df_abt, df_IceDist, how='outer', on='date')
    df_abt = pd.merge(df_abt, df_Nutrients, how='outer', on='date')
    df_abt = pd.merge(df_abt, df_past_time, how='outer', on='date')

    # dayligh is missing
    if mod != "EGC":
        df_abt = pd.merge(df_abt, df_Strat, how='outer', on='date')
        df_abt['daylight'] = np.nan
    # if no start MLD is missing
    else:
        df_abt['daylight'] = np.nan
        df_abt['MLD'] = np.nan

    # set the index to date
    df_abt = df_abt.set_index('date')
    # Placeholders for IceDistPast and IceConcPast
    # df_abt['iceDistPast'] = np.nan
    # df_abt['icePast'] = np.nan
    # sort out the other moorings

    # create the column mooring for the mean script
    if mod == "EGC":
        df_abt = df_abt[df_abt['mooring_full'].apply(is_egc)]
        df_abt['mooring'] = mod
    elif mod == "F4":
        df_abt = df_abt[df_abt['mooring_full'].apply(is_f4)]
        df_abt['mooring'] = mod
    elif mod == "HG-IV":
        df_abt = df_abt[df_abt['mooring_full'].apply(is_hg)]
        df_abt['mooring'] = mod

    df_abt = df_abt.sort_index(ascending=True)
    # save to txt
    save_path = path+f"/Full_ABT_{mod}_4_years_test.txt"
    df_abt.to_csv(save_path, sep="\t")
    return save_path


def mean_evn_data(clean_sample_info_path:str="../data/EGC_2016-2020/clean_simple_info.txt",
                  full_abt_path:str="../data/EGC_2016-2020/Full_ABT_EGC_4_years_test.txt",
                  path:str = "../data/EGC_2016-2020/",
                  ):
    sample_info = pd.read_csv(clean_sample_info_path, sep = '\t')
    df = pd.read_csv(full_abt_path, sep = '\t')

    # Initialize an empty list to store the resulting dataframes
    mean_data_list = []
    new_date = sample_info.copy()
    new_date = new_date[["date1", "date2", "date3", "date4"]]
    for da in ["date1", "date2", "date3", "date4"]:
        new_date[da] = pd.to_datetime(new_date[da])
    new_date['date'] = new_date.apply(lambda x: calculate_mean_date_over_samples(x.date1, x.date2, x.date3, x.date4),axis =1)
    new_date = new_date['date']


    for i in range(sample_info.shape[0]):
        date = pd.DataFrame(sample_info.iloc[i, 6:10]).reset_index(drop=True).dropna()
        temp_new_date = new_date.iloc[i].date()
        mooring = sample_info['mooring'][i]
        bla = df[(df['date'].apply(lambda x: x in date.values)) & df['mooring'].apply(lambda x: x == mooring)]
        mean_data = bla.groupby('mooring_full').mean(numeric_only=True).reset_index()
        mean_data['date'] =temp_new_date
        mean_data_list.append(mean_data)

    # Concatenate the dataframes into a single dataframe
    mean_data = pd.concat(mean_data_list, ignore_index=True)
    mean_data = mean_data[['date', 'depth', 'temp', 'sal', 'sig', 'O2_conc', 'O2_sat',
       'chl_sens', 'CO2', 'pH', 'AW_frac', 'PW_frac', 'iceConc', 'Chl_sat',
       'iceDist', 'NO3_NO2', 'PO4', 'NO2', 'SiO4', 'icePast', 'iceDistPast',
       'daylight', 'MLD']]

    save_csv_path  = path + "ABC_MainEGCABT_final.csv"
    mean_data.to_csv(save_csv_path, sep=";")
    return save_csv_path

def create_bac_meta(main_abt_file:str , mod = "EGC", path = "../data/EGC_2016-2020/", clean_sample_info = "../data/EGC_2016-2020/clean_simple_info.txt",):
    # data frame for Meta data

    meta_cols = ['sample_title', 'RAS_id', 'locus_tag', 'mooring_full', 'mooring',
           'year', 'month_abb', 'month', 'lat', 'lon', 'DNA_ng_uL', 'clip_id',
           'depth', 'temp', 'sal', 'sig', 'O2_conc', 'O2_sat', 'chl_sens', 'CO2',
           'pH', 'AW_frac', 'PW_frac', 'date', 'daylight', 'NO3_NO2', 'PO4', 'NO2',
           'SiO4', 'MLD', 'iceConc', 'iceDist', 'icePast', 'iceDistPast']

    #print(meta_cols)
    df_sample_info = pd.read_csv(clean_sample_info, sep="\t", engine="python")
    df_sample_info = df_sample_info[df_sample_info["mooring"] == mod]
    df_sample_info = df_sample_info.set_index("date")
    value_counts = df_sample_info.index.value_counts()
    df_sample_info = df_sample_info[df_sample_info["locus_tag"] == "16S"]

    df_Abt_mean = pd.read_csv(main_abt_file, sep=";", engine="python")
    #df_Abt_mean = df_Abt_mean.drop(columns=['Group.1'])
    df_Abt_mean["mooring"] = mod
    df_Abt_mean = df_Abt_mean.drop_duplicates(subset=['date'], keep="first")
    df_Abt_mean = df_Abt_mean.set_index("date")

    # merge the data
    df_Abt_mean = df_Abt_mean.drop(columns=['mooring'])
    df_Abt_mean = df_Abt_mean.drop(columns=['Unnamed: 0'])
    df_sample_info = df_sample_info.drop(columns=['Unnamed: 0'])

    df_bac_Meta = df_sample_info.join(df_Abt_mean, how='left', on='date')
    # add date column
    df_bac_Meta['date'] = df_bac_Meta.index
    df_bac_Meta = df_bac_Meta[meta_cols]
    save_path = path+"bacMeta.txt"
    df_bac_Meta.to_csv(save_path, sep="\t")
    return save_path


def check_files(directory, files):
    """Check if a list of files is in a given directory and return a list of missing files."""
    try:
        # Initialize an empty list to store the missing files
        missing_files = []

        # Check if each file is in the directory
        for file in files:
            if not os.path.exists(os.path.join(directory, file)):
                missing_files.append(file)

        # Return the list of missing files
        if len(missing_files) != 0:
            print('Files are missing:', missing_files)
            return False, f"Files are missing:{missing_files}"
        else:
            print('No files missing')
            return True, 0
    except Exception as e:
        print(e)
        return False, e

def create_arg_dict(
                    folder_path='../data/EGC_2016-2020/',
                    mooring='EGC',
                    ):
    arg_dict = {}
    arg_dict['folder_path'] = folder_path
    arg_dict['mooring'] = mooring
    print(arg_dict)
    return arg_dict

def get_arguments(arg_dict):
    parser = argparse.ArgumentParser()
    for ent in arg_dict.keys():
        parser.add_argument('--{}'.format(ent), type=str, default=arg_dict[ent])
    args = parser.parse_args() # "" when running on JupyterHub
    print(args)
    return args

def main(folder_path="../data/EGC_2016-2020/", mooring= "EGC"):
    print('Start')
    files = ['Nutrients.txt', 'Strat.txt', 'IceConc.txt',
             'IceDist.txt', 'IceDistPast.csv','sample_info.txt',
             'Chl_sat.txt', 'IceConcPast.csv',
             'CTD.txt']
    check_files(folder_path, files)
    clean_sample_info_path = clean_sample_info(path=folder_path)
    abt_path = create_abt_all_moor(mod= mooring,
                        path= folder_path,
                        clean_sample_info=clean_sample_info_path,)
    mean_abt = mean_evn_data(clean_sample_info_path=clean_sample_info_path,
                            full_abt_path=abt_path,
                            path=folder_path,)
    bac_meta_path = create_bac_meta(mod=mooring,
                    path=folder_path,
                    main_abt_file= mean_abt,
                    clean_sample_info=clean_sample_info_path)
    print(f'bac_meta. txt is created see {bac_meta_path}')
    print('End')
    return bac_meta_path

if __name__ == "__main__":
    # only change here folder_path and the mooring. DON'T CHANGE THE CODE
    folder_path = '../data/EGC_2016-2020/'
    mooring = 'EGC'
    arg_dict = create_arg_dict(folder_path,mooring)
    args = get_arguments(arg_dict)
    main(str(args.folder_path), str(args.mooring))

